var Zodiac      = require('../models/zodiac.js');
var Entities = require('html-entities').AllHtmlEntities;
var currentWeekNumber = require('current-week-number');
var currentWek = currentWeekNumber();  
var currentTime = new Date()
var currentYear = currentTime.getFullYear();
var sendmail = require('../helper/emailHelper');

module.exports = {
    insertZodiacsDay :function(data){
        entities = new Entities();   
        console.log('start insert day ');
        
        for (var i in data) {
                    var zodiac = new Zodiac();
                  //aqui setamos os campos do zodiac (que virá do request)
                    zodiac.name = data[i].Horoscopo.signo_nome;
                    zodiac.dataZodiac = data[i].Horoscopo.data;
                    zodiac.description = entities.decode(data[i].Horoscopo.texto).trim();
                    zodiac.week = currentWek;
                    zodiac.year = currentYear;
                    zodiac.type = 'day';
            
                    zodiac.save(function(error) {
                        if(error)
                        console.log(error);
                });
         }          
        },
    insertZodiacsWeek :function (data){
        var zodiac = new Zodiac();
        entities = new Entities();
        console.log('start insert week '+data);
        
        for (var i in data) {
            var zodiac = new Zodiac();
              //aqui setamos os campos do zodiac (que virá do request)
                zodiac.name = data[i].AstraisSemana.signo_nome;
                zodiac.dataZodiac = data[i].AstraisSemana.data;
                var description = entities.decode(data[i].AstraisSemana.texto).trim();
                description = description.replace("/\n$/","")
                zodiac.description = description;
                zodiac.week = currentWek;
                zodiac.year = currentYear;
                zodiac.type = 'week';
        
                zodiac.save(function(error) {
                    if(error)
                        res.send(error);
                });
            }
    },

    findZodiacDay : function(dataFind){
        
        var zodiacsFind = Zodiac.find({'dataZodiac':dataFind}, '-_id -__v',function(err, zodiacs) {
            if(err)
             zodiacsFind = null;
             zodiacsFind = zodiacs;
        });
        console.log(zodiacsFind[0]);
        return zodiacsFind;
    }
}