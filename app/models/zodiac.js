var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ZodiacSchema = new Schema({
    dataZodiac: String,
    name: String,
    description: String,
    week: String,
    year: String,
    type:String
});


module.exports = mongoose.model('Zodiac', ZodiacSchema);