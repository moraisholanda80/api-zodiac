
/** 
 * Arquivo: server.js
 * Descrição: Arquivo responsável por levantar o serviço do Node.Js para
 * execução da aplicação e a API através do Express.Js.
 * Author: Sergio holanda
 * Data de Criação: 23/11/2017
 */

//Configuração Base da Aplicação:
//====================================================================================

/* Chamada das Packages que iremos precisar para a nossa aplicação */
var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var mongoose    = require('mongoose');
var Zodiac      = require('./app/models/zodiac');
var repository      = require('./app/repository/zodiacsRepository');
var expressListRoutes   = require('express-list-routes');
var Client = require('node-rest-client').Client;
var currentWeekNumber = require('current-week-number');
var Entities = require('html-entities').AllHtmlEntities;
var Probe = require('pmx').probe();
var schedule = require('node-schedule');
var dateFormat = require('dateformat');
require('dotenv').config('/Users/josesergiomorais/development/api-zodiac/api-zodiac/env');
var counter = 0;
var currentWek = currentWeekNumber();  
var currentTime = new Date()
var currentYear = currentTime.getFullYear();

var client = new Client();
var dbURL = 'mongodb://prod-api-zodiac:27017/db-zodiac';
console.log(dbURL)
var dbAuth = { 
	useMongoClient: false
}

 mongoose.Promise = global.Promise;
 mongoose.connect(dbURL, {
    
   keepAlive: true,
   reconnectTries: Number.MAX_VALUE,
   useMongoClient: true });
const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
console.log('Connected to MongoDB');
});

//Dashboard realtime
var metric = Probe.metric({
    name    : 'Counter',
    value   : function() {
      return counter;
    }
  });
  
  setInterval(function() {
    counter++;
  }, 100);
/** Configuração da variável 'app' para usar o 'bodyParser()'.
 * Ao fazermos isso nos permitirá retornar os dados a partir de um POST
 */
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

/** Definição da porta onde será executada a nossa aplicação */
var port = process.env.PORT || 8000;

//Rotas da nossa API: 
//==============================================================

/* Aqui o 'router' irá pegar as instâncias das Rotas do Express */
var router = express.Router();

/* Middleware para usar em todos os requests enviados para a nossa API- Mensagem Padrão */
router.use(function(req, res, next) {
    console.log('Request endpoint........'+ req.url);
    
    next(); //aqui é para sinalizar de que prosseguiremos para a próxima rota. E que não irá parar por aqui!!!
});

/* Rota de Teste para sabermos se tudo está realmente funcionando (acessar através: GET: http://localhost:8000/api) */
router.get('/', function(req, res) {
    res.json({ message: 'YEAH! Seja Bem-Vindo a nossa API' });
});

router.route('/zodiac')
    /* 1) Método: Criar Usuario (acessar em: POST http://localhost:8080/api/zodiac */
    .post(function(req, res) {
        var zodiac = new Zodiac();

        //aqui setamos os campos do usuario (que virá do request)
        zodiac.signo_nome = req.body.signo_nome;
        zodiac.data = req.body.data;
        zodiac.texto = req.body.texto;

        zodiac.save(function(error) {
            if(error)
                res.send(error);
                        
            res.json({ message: 'Zodiac criado!' });
        });
    })
    .get(function(req, res) {
                //Função para Selecionar Todos os 'signos' e verificar se há algum erro:
                Zodiac.find({}, '-_id -__v',function(err, zodiacs) {
                    if(err)
                        res.send(err);
        
                    res.json(zodiacs);
                });
   });
   router.route('/zodiac/week')
       /*Método: Search for week (acessar em: GET http://localhost:8080/api/zodiac/:week) */
       .get(function(req, res) {
        Zodiac.find({'week': currentWek ,'year':currentYear,'type':'week'},'-_id -__v', function(error, zodiacs) {
            if(error) 
            res.send(error);
            console.log('year '+currentYear);
            console.log('week '+currentWek);
            res.json(zodiacs);
        });           
    });
    router.route('/zodiac/day')
         /*Método: Search for week (acessar em: GET http://localhost:8080/api/zodiac/:week) */
        .get(function(req, res) {
         Zodiac.find({'week': currentWek ,'year':currentYear,'type':'day'},'-_id -__v', function(error, zodiacs) {
             if(error) 
             res.send(error);
             res.json(zodiacs);
        });           
    });
    function runCrawlerDay (res){
        client.get("http://isabelmueller.com.br/signo/horoscopos/json/", function (data, response) {    
            insertZodiacs(data,res);
          });
    }
    function runCrawlerWeek(res){
        client.get("http://isabelmueller.com.br/signo/astrais_semanas/json/", function (data, response) {    
            insertZodiacsWeek(data,res);
        });
    }
    function insertZodiacs(data,res){
        repository.insertZodiacsDay(data);
    }

    function insertZodiacsWeek(data,res){
        repository.insertZodiacsWeek(data);
    }
       
  router.route('/crawler/day')
   .post(function(req, res){
    client.get("http://isabelmueller.com.br/signo/horoscopos/json/"+currentYear+'/'+currentWek, function (data, response) {    
        var zodiac = new Zodiac();
        entities = new Entities();
        db.collection("zodiacs").remove({},function(err,numberRemoved){
            console.log("inside remove call back" + numberRemoved);
        });
       
        for (var i in data) {
            var zodiac = new Zodiac();
                  //aqui setamos os campos do zodiac (que virá do request)
                    zodiac.name = data[i].Horoscopo.signo_nome;
                    zodiac.dataZodiac = data[i].Horoscopo.data;
                    zodiac.description = entities.decode(data[i].Horoscopo.texto).trim();
                    zodiac.week = currentWek;
                    zodiac.year = currentYear;
                    zodiac.type = 'day';
            
                    zodiac.save(function(error) {
                        if(error)
                            res.send(error);
                            res.json({ message: 'Zodiac create!' });
                });
         }
      });
    });
    router.route('/crawler/week')
    .post(function(req, res){
     client.get("http://isabelmueller.com.br/signo/astrais_semanas/json/"+currentYear+'/'+currentWek, function (data, response) {    
         var zodiac = new Zodiac();
         entities = new Entities();
        
         for (var i in data) {
             var zodiac = new Zodiac();
                   //aqui setamos os campos do zodiac (que virá do request)
                     zodiac.name = data[i].AstraisSemana.signo_nome;
                     zodiac.data = data[i].AstraisSemana.data;
                     var description = entities.decode(data[i].AstraisSemana.texto).trim();
                     description = description.replace("/\n$/","")
                     zodiac.description = description;
                     zodiac.week = currentWek;
                     zodiac.year = currentYear;
                     zodiac.type = 'week';
             
                     zodiac.save(function(error) {
                         if(error)
                             res.send(error);
                        res.json({ message: 'Zodiac create!' });
                });
            }
       });
    });

/* Todas as nossas rotas serão prefixadas com '/api' */
app.use('/api', router);
var dateFind = dateFormat(currentTime, "isoDate");

expressListRoutes({ prefix: '/api' }, 'API:', router );
var crawlerWeek = schedule.scheduleJob('*/30 0,08-23 * * *', function(){
    console.log('week '+currentWek);
    Zodiac.find({'week':currentWek,'type':'week'}, '-_id -__v',function(err, zodiacs) {
        if(zodiacs.length==0)
            runCrawlerWeek();
    });
});
var crawlerDay = schedule.scheduleJob('*/30 0,08-22 * * *', function(){
    Zodiac.find({'dataZodiac':dateFind}, '-_id -__v',function(err, zodiacs) {
       if(zodiacs.length==0)
        runCrawlerDay();
    });
});

//Iniciando o Servidor (Aplicação):
//==============================================================
app.listen(port);
console.log('Start application ' +require(__dirname + '/package.json').name +' on port '+port);


